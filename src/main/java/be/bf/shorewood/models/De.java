package be.bf.shorewood.models;

import java.security.SecureRandom;

public class De {
    private int min;
    private int max;

    private SecureRandom secureRandom = new SecureRandom();

    public De(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public int lance() {
        return secureRandom.nextInt(this.min, this.max + 1); // bound excepted
    }

    public int lance(int nb, int garder) {
        int total = 0;
        int[] tries = new int[nb];
        for(int i = 0; i < nb; i++) {
            tries[i] = lance();
        }

        triDesc(tries);

        for(int i = 0; i < garder; i++) {
            total += tries[i];
        }

        return total;
    }

    private void triDesc(int[] lance) {
        for(int i = 0; i < lance.length - 1; i++) {
            int iMax = i;
            for(int j = i + 1; j < lance.length; j++) {
                if (lance[iMax] < lance[j]) {
                    iMax = j;
                }
            }
            if (iMax != i) {
                int tmp = lance[i];
                lance[i] = lance[iMax];
                lance[iMax] = tmp;
            }
        }
    }
}
