package be.bf.shorewood.models;

public class Personnage {
    private int pvMax;
    private int pv;
    private int force;
    private int end;
    private String nom;

    public Personnage(String nom, int force, int end) {
        De de = new De(1,6);

        this.force = de.lance(4,3) + force;
        this.end = de.lance(4,3) + end;

        this.pv = this.pvMax = this.force + this.end + this.modifier(this.end);
        this.nom = nom;
    }

    private int modifier(int stat) {
        if (stat >= 15) return 2;
        if (stat >= 10) return 1;
        if (stat >= 5) return 0;
        return -1;
    }

    public void frappe(Personnage target) {
        De de = new De(1,4);
        int dommage = de.lance() + modifier(this.force);

        target.pv -= dommage;

        if (target.pv <= 0) {
            System.out.printf("%s est decedee\n", target.nom);
        }
    }
    public boolean estEnVie() {
        return this.pv > 0;
    }

    protected void soin() {
        this.pv = this.pvMax;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("Personnage{");

        builder.append("pvMax= ").append(pvMax).append(", ");
        builder.append("pv= ").append(pv).append(", ");
        builder.append("force= ").append(force).append(", ");
        builder.append("end= ").append(end);

        return builder.append("}").toString();
    }
}
