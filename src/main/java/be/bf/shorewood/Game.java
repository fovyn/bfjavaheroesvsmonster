package be.bf.shorewood;

import be.bf.shorewood.models.*;

import java.util.Scanner;

public class Game {
    public static void main(String[] args) {
        Hero humain = new Humain();

        Scanner scanner = new Scanner(System.in);
        De monsterGen = new De(1,3);

        System.out.println("Continuer (y/q)");
        while (humain.estEnVie() && !scanner.next().equals("q")) {
            Personnage target = switch (monsterGen.lance()) {
                case 1 -> new Loup();
                case 2 -> new Orc();
                default -> new Dragonnet();
            };

            while (humain.estEnVie() && target.estEnVie()) {
                humain.frappe(target);
                if (target.estEnVie()) {
                    target.frappe(humain);
                }
            }

            if (humain.estEnVie()) {
                humain.soin();
                System.out.println("Continuer (y/q)");
            }
        }
    }
}
